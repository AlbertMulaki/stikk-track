﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Track.BOL;
using Track.DAL;
using System.Data;
namespace Track.BLL
{
    
    public class ItemsBLL
    {
        private DALItems itemDAL;

        public ItemsBLL()
        {
            itemDAL = new DALItems();
        }
        public bool Insert(Items item)
        {   
            if(item.Name !=null && item.Barcode != null)
            {
               // return itemDAL.Insert(item);
            }
            else
            {
                //return false;
            }
            return false;
            
        }
        public DataTable GetBrands()
        {
            return itemDAL.GetBrands();
        }
        public DataTable GetUnits()
        {
            return itemDAL.GetUnits();
        }
        public DataTable GetTypeOfGoods()
        {
            return itemDAL.GetTypeOfGoofs();
        }
        public DataTable GetTaxRates()
        {
            return itemDAL.GetTaxRates();
        }
        public DataTable GetItemCategory()
        {
            return itemDAL.GetItemCategory();
        }
        public DataTable GetSubCategory()
        {
            return itemDAL.GetSubCategory();
        }
        public DataTable GetAccount()
        {
            return itemDAL.GetAccount();
        }

    }
}
