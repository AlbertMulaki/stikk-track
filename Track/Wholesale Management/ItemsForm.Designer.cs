﻿namespace Wholesale_Management
{
    partial class ItemsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblItemNumber = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.txtItemID = new System.Windows.Forms.TextBox();
            this.comboBoxBrand = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxIncomeAcc = new System.Windows.Forms.ComboBox();
            this.lblIncomeAccount = new System.Windows.Forms.Label();
            this.comboBoxExpenseAcc = new System.Windows.Forms.ComboBox();
            this.lblExpenseAccount = new System.Windows.Forms.Label();
            this.comboBoxTaxRates = new System.Windows.Forms.ComboBox();
            this.lblTaxRates = new System.Windows.Forms.Label();
            this.comboBoxTypeofGoods = new System.Windows.Forms.ComboBox();
            this.lblTypeofGoods = new System.Windows.Forms.Label();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.lblHeight = new System.Windows.Forms.Label();
            this.txtWidth = new System.Windows.Forms.TextBox();
            this.lblWidth = new System.Windows.Forms.Label();
            this.txtLength = new System.Windows.Forms.TextBox();
            this.lblLength = new System.Windows.Forms.Label();
            this.txtGrossWeight = new System.Windows.Forms.TextBox();
            this.lblGrossWeight = new System.Windows.Forms.Label();
            this.txtNetWeight = new System.Windows.Forms.TextBox();
            this.lblNetWeight = new System.Windows.Forms.Label();
            this.checkBoxStatus = new System.Windows.Forms.CheckBox();
            this.comboBoxItemCategory2 = new System.Windows.Forms.ComboBox();
            this.lblItemCategory2 = new System.Windows.Forms.Label();
            this.comboBoxItemCategory = new System.Windows.Forms.ComboBox();
            this.lblItemCategory1 = new System.Windows.Forms.Label();
            this.lblUnit = new System.Windows.Forms.Label();
            this.comboBoxUnit = new System.Windows.Forms.ComboBox();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.groupBoxItemInfo = new System.Windows.Forms.GroupBox();
            this.txtPackage = new System.Windows.Forms.TextBox();
            this.lblPackage = new System.Windows.Forms.Label();
            this.groupBoxWeightDimensions = new System.Windows.Forms.GroupBox();
            this.groupBoxItemConfiguration = new System.Windows.Forms.GroupBox();
            this.groupBoxAccounting = new System.Windows.Forms.GroupBox();
            this.lblCostAcc = new System.Windows.Forms.Label();
            this.comboBoxCostAcc = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxItemInfo.SuspendLayout();
            this.groupBoxWeightDimensions.SuspendLayout();
            this.groupBoxItemConfiguration.SuspendLayout();
            this.groupBoxAccounting.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblItemNumber
            // 
            this.lblItemNumber.AutoSize = true;
            this.lblItemNumber.Location = new System.Drawing.Point(6, 22);
            this.lblItemNumber.Name = "lblItemNumber";
            this.lblItemNumber.Size = new System.Drawing.Size(70, 13);
            this.lblItemNumber.TabIndex = 0;
            this.lblItemNumber.Text = "Item Number:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(6, 48);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Name:";
            // 
            // txtItemID
            // 
            this.txtItemID.Location = new System.Drawing.Point(100, 19);
            this.txtItemID.Name = "txtItemID";
            this.txtItemID.Size = new System.Drawing.Size(121, 20);
            this.txtItemID.TabIndex = 2;
            // 
            // comboBoxBrand
            // 
            this.comboBoxBrand.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxBrand.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxBrand.FormattingEnabled = true;
            this.comboBoxBrand.Location = new System.Drawing.Point(106, 127);
            this.comboBoxBrand.Name = "comboBoxBrand";
            this.comboBoxBrand.Size = new System.Drawing.Size(121, 21);
            this.comboBoxBrand.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 33;
            this.label5.Text = "Brand:";
            // 
            // comboBoxIncomeAcc
            // 
            this.comboBoxIncomeAcc.FormattingEnabled = true;
            this.comboBoxIncomeAcc.Location = new System.Drawing.Point(119, 46);
            this.comboBoxIncomeAcc.Name = "comboBoxIncomeAcc";
            this.comboBoxIncomeAcc.Size = new System.Drawing.Size(177, 21);
            this.comboBoxIncomeAcc.TabIndex = 32;
            // 
            // lblIncomeAccount
            // 
            this.lblIncomeAccount.AutoSize = true;
            this.lblIncomeAccount.Location = new System.Drawing.Point(6, 49);
            this.lblIncomeAccount.Name = "lblIncomeAccount";
            this.lblIncomeAccount.Size = new System.Drawing.Size(87, 13);
            this.lblIncomeAccount.TabIndex = 31;
            this.lblIncomeAccount.Text = "Income account:";
            // 
            // comboBoxExpenseAcc
            // 
            this.comboBoxExpenseAcc.FormattingEnabled = true;
            this.comboBoxExpenseAcc.Location = new System.Drawing.Point(119, 19);
            this.comboBoxExpenseAcc.Name = "comboBoxExpenseAcc";
            this.comboBoxExpenseAcc.Size = new System.Drawing.Size(177, 21);
            this.comboBoxExpenseAcc.TabIndex = 30;
            // 
            // lblExpenseAccount
            // 
            this.lblExpenseAccount.AutoSize = true;
            this.lblExpenseAccount.Location = new System.Drawing.Point(6, 22);
            this.lblExpenseAccount.Name = "lblExpenseAccount";
            this.lblExpenseAccount.Size = new System.Drawing.Size(93, 13);
            this.lblExpenseAccount.TabIndex = 29;
            this.lblExpenseAccount.Text = "Expense account:";
            // 
            // comboBoxTaxRates
            // 
            this.comboBoxTaxRates.FormattingEnabled = true;
            this.comboBoxTaxRates.Location = new System.Drawing.Point(106, 46);
            this.comboBoxTaxRates.Name = "comboBoxTaxRates";
            this.comboBoxTaxRates.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTaxRates.TabIndex = 26;
            // 
            // lblTaxRates
            // 
            this.lblTaxRates.AutoSize = true;
            this.lblTaxRates.Location = new System.Drawing.Point(6, 48);
            this.lblTaxRates.Name = "lblTaxRates";
            this.lblTaxRates.Size = new System.Drawing.Size(59, 13);
            this.lblTaxRates.TabIndex = 25;
            this.lblTaxRates.Text = "Tax Rates:";
            // 
            // comboBoxTypeofGoods
            // 
            this.comboBoxTypeofGoods.FormattingEnabled = true;
            this.comboBoxTypeofGoods.Location = new System.Drawing.Point(106, 19);
            this.comboBoxTypeofGoods.Name = "comboBoxTypeofGoods";
            this.comboBoxTypeofGoods.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTypeofGoods.TabIndex = 24;
            // 
            // lblTypeofGoods
            // 
            this.lblTypeofGoods.AutoSize = true;
            this.lblTypeofGoods.Location = new System.Drawing.Point(6, 22);
            this.lblTypeofGoods.Name = "lblTypeofGoods";
            this.lblTypeofGoods.Size = new System.Drawing.Size(78, 13);
            this.lblTypeofGoods.TabIndex = 23;
            this.lblTypeofGoods.Text = "Type of goods:";
            // 
            // txtHeight
            // 
            this.txtHeight.Location = new System.Drawing.Point(100, 123);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(121, 20);
            this.txtHeight.TabIndex = 21;
            // 
            // lblHeight
            // 
            this.lblHeight.AutoSize = true;
            this.lblHeight.Location = new System.Drawing.Point(6, 126);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(41, 13);
            this.lblHeight.TabIndex = 20;
            this.lblHeight.Text = "Height:";
            // 
            // txtWidth
            // 
            this.txtWidth.Location = new System.Drawing.Point(100, 97);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(121, 20);
            this.txtWidth.TabIndex = 19;
            // 
            // lblWidth
            // 
            this.lblWidth.AutoSize = true;
            this.lblWidth.Location = new System.Drawing.Point(6, 100);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(38, 13);
            this.lblWidth.TabIndex = 18;
            this.lblWidth.Text = "Width:";
            // 
            // txtLength
            // 
            this.txtLength.Location = new System.Drawing.Point(100, 71);
            this.txtLength.Name = "txtLength";
            this.txtLength.Size = new System.Drawing.Size(121, 20);
            this.txtLength.TabIndex = 17;
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(6, 74);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(43, 13);
            this.lblLength.TabIndex = 16;
            this.lblLength.Text = "Length:";
            // 
            // txtGrossWeight
            // 
            this.txtGrossWeight.Location = new System.Drawing.Point(100, 45);
            this.txtGrossWeight.Name = "txtGrossWeight";
            this.txtGrossWeight.Size = new System.Drawing.Size(121, 20);
            this.txtGrossWeight.TabIndex = 15;
            // 
            // lblGrossWeight
            // 
            this.lblGrossWeight.AutoSize = true;
            this.lblGrossWeight.Location = new System.Drawing.Point(6, 48);
            this.lblGrossWeight.Name = "lblGrossWeight";
            this.lblGrossWeight.Size = new System.Drawing.Size(74, 13);
            this.lblGrossWeight.TabIndex = 14;
            this.lblGrossWeight.Text = "Gross Weight:";
            // 
            // txtNetWeight
            // 
            this.txtNetWeight.Location = new System.Drawing.Point(100, 19);
            this.txtNetWeight.Name = "txtNetWeight";
            this.txtNetWeight.Size = new System.Drawing.Size(121, 20);
            this.txtNetWeight.TabIndex = 13;
            // 
            // lblNetWeight
            // 
            this.lblNetWeight.AutoSize = true;
            this.lblNetWeight.Location = new System.Drawing.Point(6, 22);
            this.lblNetWeight.Name = "lblNetWeight";
            this.lblNetWeight.Size = new System.Drawing.Size(64, 13);
            this.lblNetWeight.TabIndex = 12;
            this.lblNetWeight.Text = "Net Weight:";
            // 
            // checkBoxStatus
            // 
            this.checkBoxStatus.AutoSize = true;
            this.checkBoxStatus.Location = new System.Drawing.Point(227, 21);
            this.checkBoxStatus.Name = "checkBoxStatus";
            this.checkBoxStatus.Size = new System.Drawing.Size(63, 17);
            this.checkBoxStatus.TabIndex = 22;
            this.checkBoxStatus.Text = "Passive";
            this.checkBoxStatus.UseVisualStyleBackColor = true;
            // 
            // comboBoxItemCategory2
            // 
            this.comboBoxItemCategory2.FormattingEnabled = true;
            this.comboBoxItemCategory2.Location = new System.Drawing.Point(106, 100);
            this.comboBoxItemCategory2.Name = "comboBoxItemCategory2";
            this.comboBoxItemCategory2.Size = new System.Drawing.Size(121, 21);
            this.comboBoxItemCategory2.TabIndex = 11;
            // 
            // lblItemCategory2
            // 
            this.lblItemCategory2.AutoSize = true;
            this.lblItemCategory2.Location = new System.Drawing.Point(6, 103);
            this.lblItemCategory2.Name = "lblItemCategory2";
            this.lblItemCategory2.Size = new System.Drawing.Size(74, 13);
            this.lblItemCategory2.TabIndex = 10;
            this.lblItemCategory2.Text = "Sub Category:";
            // 
            // comboBoxItemCategory
            // 
            this.comboBoxItemCategory.FormattingEnabled = true;
            this.comboBoxItemCategory.Location = new System.Drawing.Point(106, 73);
            this.comboBoxItemCategory.Name = "comboBoxItemCategory";
            this.comboBoxItemCategory.Size = new System.Drawing.Size(121, 21);
            this.comboBoxItemCategory.TabIndex = 9;
            // 
            // lblItemCategory1
            // 
            this.lblItemCategory1.AutoSize = true;
            this.lblItemCategory1.Location = new System.Drawing.Point(6, 76);
            this.lblItemCategory1.Name = "lblItemCategory1";
            this.lblItemCategory1.Size = new System.Drawing.Size(75, 13);
            this.lblItemCategory1.TabIndex = 8;
            this.lblItemCategory1.Text = "Item Category:";
            // 
            // lblUnit
            // 
            this.lblUnit.AutoSize = true;
            this.lblUnit.Location = new System.Drawing.Point(6, 100);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(29, 13);
            this.lblUnit.TabIndex = 7;
            this.lblUnit.Text = "Unit:";
            // 
            // comboBoxUnit
            // 
            this.comboBoxUnit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.comboBoxUnit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxUnit.FormattingEnabled = true;
            this.comboBoxUnit.Location = new System.Drawing.Point(100, 97);
            this.comboBoxUnit.Name = "comboBoxUnit";
            this.comboBoxUnit.Size = new System.Drawing.Size(121, 21);
            this.comboBoxUnit.TabIndex = 6;
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(100, 71);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(190, 20);
            this.txtBarcode.TabIndex = 5;
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.Location = new System.Drawing.Point(6, 74);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(50, 13);
            this.lblBarcode.TabIndex = 4;
            this.lblBarcode.Text = "Barcode:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(100, 45);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(190, 20);
            this.txtName.TabIndex = 3;
            // 
            // groupBoxItemInfo
            // 
            this.groupBoxItemInfo.Controls.Add(this.txtPackage);
            this.groupBoxItemInfo.Controls.Add(this.lblPackage);
            this.groupBoxItemInfo.Controls.Add(this.txtItemID);
            this.groupBoxItemInfo.Controls.Add(this.lblItemNumber);
            this.groupBoxItemInfo.Controls.Add(this.txtName);
            this.groupBoxItemInfo.Controls.Add(this.lblName);
            this.groupBoxItemInfo.Controls.Add(this.comboBoxUnit);
            this.groupBoxItemInfo.Controls.Add(this.lblUnit);
            this.groupBoxItemInfo.Controls.Add(this.checkBoxStatus);
            this.groupBoxItemInfo.Controls.Add(this.txtBarcode);
            this.groupBoxItemInfo.Controls.Add(this.lblBarcode);
            this.groupBoxItemInfo.Location = new System.Drawing.Point(3, 3);
            this.groupBoxItemInfo.Name = "groupBoxItemInfo";
            this.groupBoxItemInfo.Size = new System.Drawing.Size(301, 157);
            this.groupBoxItemInfo.TabIndex = 0;
            this.groupBoxItemInfo.TabStop = false;
            this.groupBoxItemInfo.Text = "Item Info";
            // 
            // txtPackage
            // 
            this.txtPackage.Location = new System.Drawing.Point(100, 124);
            this.txtPackage.Name = "txtPackage";
            this.txtPackage.Size = new System.Drawing.Size(121, 20);
            this.txtPackage.TabIndex = 24;
            // 
            // lblPackage
            // 
            this.lblPackage.AutoSize = true;
            this.lblPackage.Location = new System.Drawing.Point(6, 127);
            this.lblPackage.Name = "lblPackage";
            this.lblPackage.Size = new System.Drawing.Size(53, 13);
            this.lblPackage.TabIndex = 23;
            this.lblPackage.Text = "Package:";
            // 
            // groupBoxWeightDimensions
            // 
            this.groupBoxWeightDimensions.Controls.Add(this.lblHeight);
            this.groupBoxWeightDimensions.Controls.Add(this.lblNetWeight);
            this.groupBoxWeightDimensions.Controls.Add(this.txtNetWeight);
            this.groupBoxWeightDimensions.Controls.Add(this.lblGrossWeight);
            this.groupBoxWeightDimensions.Controls.Add(this.txtGrossWeight);
            this.groupBoxWeightDimensions.Controls.Add(this.lblLength);
            this.groupBoxWeightDimensions.Controls.Add(this.txtLength);
            this.groupBoxWeightDimensions.Controls.Add(this.lblWidth);
            this.groupBoxWeightDimensions.Controls.Add(this.txtWidth);
            this.groupBoxWeightDimensions.Controls.Add(this.txtHeight);
            this.groupBoxWeightDimensions.Location = new System.Drawing.Point(3, 166);
            this.groupBoxWeightDimensions.Name = "groupBoxWeightDimensions";
            this.groupBoxWeightDimensions.Size = new System.Drawing.Size(232, 156);
            this.groupBoxWeightDimensions.TabIndex = 2;
            this.groupBoxWeightDimensions.TabStop = false;
            this.groupBoxWeightDimensions.Text = "Item weight and dimensions ";
            // 
            // groupBoxItemConfiguration
            // 
            this.groupBoxItemConfiguration.Controls.Add(this.lblTypeofGoods);
            this.groupBoxItemConfiguration.Controls.Add(this.comboBoxBrand);
            this.groupBoxItemConfiguration.Controls.Add(this.comboBoxTypeofGoods);
            this.groupBoxItemConfiguration.Controls.Add(this.label5);
            this.groupBoxItemConfiguration.Controls.Add(this.lblTaxRates);
            this.groupBoxItemConfiguration.Controls.Add(this.comboBoxTaxRates);
            this.groupBoxItemConfiguration.Controls.Add(this.comboBoxItemCategory);
            this.groupBoxItemConfiguration.Controls.Add(this.comboBoxItemCategory2);
            this.groupBoxItemConfiguration.Controls.Add(this.lblItemCategory2);
            this.groupBoxItemConfiguration.Controls.Add(this.lblItemCategory1);
            this.groupBoxItemConfiguration.Location = new System.Drawing.Point(310, 3);
            this.groupBoxItemConfiguration.Name = "groupBoxItemConfiguration";
            this.groupBoxItemConfiguration.Size = new System.Drawing.Size(240, 157);
            this.groupBoxItemConfiguration.TabIndex = 1;
            this.groupBoxItemConfiguration.TabStop = false;
            this.groupBoxItemConfiguration.Text = "Item Configuration";
            // 
            // groupBoxAccounting
            // 
            this.groupBoxAccounting.Controls.Add(this.lblCostAcc);
            this.groupBoxAccounting.Controls.Add(this.comboBoxCostAcc);
            this.groupBoxAccounting.Controls.Add(this.comboBoxExpenseAcc);
            this.groupBoxAccounting.Controls.Add(this.lblExpenseAccount);
            this.groupBoxAccounting.Controls.Add(this.lblIncomeAccount);
            this.groupBoxAccounting.Controls.Add(this.comboBoxIncomeAcc);
            this.groupBoxAccounting.Location = new System.Drawing.Point(241, 166);
            this.groupBoxAccounting.Name = "groupBoxAccounting";
            this.groupBoxAccounting.Size = new System.Drawing.Size(309, 102);
            this.groupBoxAccounting.TabIndex = 3;
            this.groupBoxAccounting.TabStop = false;
            this.groupBoxAccounting.Text = "Accounting Configuration";
            // 
            // lblCostAcc
            // 
            this.lblCostAcc.AutoSize = true;
            this.lblCostAcc.Location = new System.Drawing.Point(6, 76);
            this.lblCostAcc.Name = "lblCostAcc";
            this.lblCostAcc.Size = new System.Drawing.Size(73, 13);
            this.lblCostAcc.TabIndex = 33;
            this.lblCostAcc.Text = "Cost account:";
            // 
            // comboBoxCostAcc
            // 
            this.comboBoxCostAcc.FormattingEnabled = true;
            this.comboBoxCostAcc.Location = new System.Drawing.Point(119, 73);
            this.comboBoxCostAcc.Name = "comboBoxCostAcc";
            this.comboBoxCostAcc.Size = new System.Drawing.Size(177, 21);
            this.comboBoxCostAcc.TabIndex = 34;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(412, 283);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(147, 48);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(250, 283);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(147, 48);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBoxItemInfo);
            this.panel1.Controls.Add(this.groupBoxWeightDimensions);
            this.panel1.Controls.Add(this.groupBoxAccounting);
            this.panel1.Controls.Add(this.groupBoxItemConfiguration);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(556, 328);
            this.panel1.TabIndex = 10;
            // 
            // ItemsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 352);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panel1);
            this.Name = "ItemsForm";
            this.Text = "ItemsForm";
            this.Load += new System.EventHandler(this.ItemsForm_Load);
            this.groupBoxItemInfo.ResumeLayout(false);
            this.groupBoxItemInfo.PerformLayout();
            this.groupBoxWeightDimensions.ResumeLayout(false);
            this.groupBoxWeightDimensions.PerformLayout();
            this.groupBoxItemConfiguration.ResumeLayout(false);
            this.groupBoxItemConfiguration.PerformLayout();
            this.groupBoxAccounting.ResumeLayout(false);
            this.groupBoxAccounting.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblItemNumber;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtItemID;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.ComboBox comboBoxUnit;
        private System.Windows.Forms.ComboBox comboBoxItemCategory;
        private System.Windows.Forms.Label lblItemCategory1;
        private System.Windows.Forms.ComboBox comboBoxItemCategory2;
        private System.Windows.Forms.Label lblItemCategory2;
        private System.Windows.Forms.TextBox txtNetWeight;
        private System.Windows.Forms.Label lblNetWeight;
        private System.Windows.Forms.TextBox txtLength;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.TextBox txtGrossWeight;
        private System.Windows.Forms.Label lblGrossWeight;
        private System.Windows.Forms.CheckBox checkBoxStatus;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.TextBox txtWidth;
        private System.Windows.Forms.Label lblWidth;
        private System.Windows.Forms.ComboBox comboBoxTaxRates;
        private System.Windows.Forms.Label lblTaxRates;
        private System.Windows.Forms.ComboBox comboBoxTypeofGoods;
        private System.Windows.Forms.Label lblTypeofGoods;
        private System.Windows.Forms.ComboBox comboBoxExpenseAcc;
        private System.Windows.Forms.Label lblExpenseAccount;
        private System.Windows.Forms.ComboBox comboBoxIncomeAcc;
        private System.Windows.Forms.Label lblIncomeAccount;
        private System.Windows.Forms.ComboBox comboBoxBrand;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBoxItemInfo;
        private System.Windows.Forms.TextBox txtPackage;
        private System.Windows.Forms.Label lblPackage;
        private System.Windows.Forms.GroupBox groupBoxWeightDimensions;
        private System.Windows.Forms.GroupBox groupBoxItemConfiguration;
        private System.Windows.Forms.GroupBox groupBoxAccounting;
        private System.Windows.Forms.Label lblCostAcc;
        private System.Windows.Forms.ComboBox comboBoxCostAcc;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panel1;
    }
}