﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Track.BOL;
using Track.BLL;


namespace Wholesale_Management
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {

            new ItemsForm().ShowDialog();

        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSignIn;
        }
    }
}
