﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Wholesale_Management
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void newBrandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NewBrandForm().ShowDialog();
        }

        private void viewBrandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ViewBrandForm().ShowDialog();
        }

        private void itemsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new ItemsForm().ShowDialog();
        }
    }
}
