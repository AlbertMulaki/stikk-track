﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Track.BOL;
using Track.BLL;

namespace Wholesale_Management
{
    public partial class ItemsForm : Form
    {
        private ItemsBLL itemsBLL;
        
        public ItemsForm()
        {
            InitializeComponent();
        }

        private void ItemsForm_Load(object sender, EventArgs e)
        {
            itemsBLL = new ItemsBLL();

            comboBoxBrand.DataSource =  itemsBLL.GetBrands();
            comboBoxBrand.DisplayMember = "Name";
            comboBoxBrand.ValueMember = "BrandID";

            comboBoxUnit.DataSource = itemsBLL.GetUnits();
            comboBoxUnit.DisplayMember = "Unit";
            comboBoxUnit.ValueMember = "UnitID";

            comboBoxTypeofGoods.DataSource = itemsBLL.GetTypeOfGoods();
            comboBoxTypeofGoods.DisplayMember = "Name";
            comboBoxTypeofGoods.ValueMember = "TypeOfGoodsID";

            comboBoxTaxRates.DataSource = itemsBLL.GetTaxRates();
            comboBoxTaxRates.DisplayMember = "Name";
            comboBoxTaxRates.ValueMember = "TaxRatesID";

            comboBoxItemCategory.DataSource = itemsBLL.GetItemCategory();
            comboBoxItemCategory.DisplayMember = "Name";
            comboBoxItemCategory.ValueMember = "ItemCategoryID";

            comboBoxItemCategory2.DataSource = itemsBLL.GetSubCategory();
            comboBoxItemCategory2.DisplayMember = "Name";
            comboBoxItemCategory2.ValueMember = "ItemSubCateogryID";

            comboBoxBrand.DataSource = itemsBLL.GetBrands();
            comboBoxBrand.DisplayMember = "Name";
            comboBoxBrand.ValueMember = "BrandID";

            comboBoxExpenseAcc.DataSource = itemsBLL.GetAccount();
            comboBoxExpenseAcc.DisplayMember = "Description";
            comboBoxExpenseAcc.ValueMember = "AccountNumberID";

            comboBoxIncomeAcc.DataSource = itemsBLL.GetAccount();
            comboBoxIncomeAcc.DisplayMember = "Description";
            comboBoxIncomeAcc.ValueMember = "AccountNumberID";

            comboBoxCostAcc.DataSource = itemsBLL.GetAccount();
            comboBoxCostAcc.DisplayMember = "Description";
            comboBoxCostAcc.ValueMember = "AccountNumberID";

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Items item = new Items();
            try
            {
                item.Name = txtName.Text;
                item.Barcode = txtBarcode.Text;
                item.BasicMeasuringUnitID = Convert.ToInt32( comboBoxUnit.SelectedValue);
                item.SubCategoryID = Convert.ToInt32(comboBoxItemCategory2.SelectedValue);
                double package;
                double.TryParse(txtPackage.Text, out package);
                item.Package = package;

                double netweight;
                double.TryParse(txtNetWeight.Text, out netweight);
                item.NetWeight = netweight;

                double grossweight;
                double.TryParse(txtGrossWeight.Text, out grossweight);
                item.GrossWeight = grossweight;

                double length;
                double.TryParse(txtLength.Text, out length);
                item.Length = length;

                double width;
                double.TryParse(txtWidth.Text, out width);
                item.Width = width;

                double height;
                double.TryParse(txtHeight.Text, out height);
                item.Height = height;
                
                
                item.TypeOfGoods = Convert.ToInt32(comboBoxTypeofGoods.SelectedValue);
                item.TaxRates = comboBoxTaxRates.Text;
                item.ExpenseAccount = comboBoxExpenseAcc.Text;
                item.SalesAccount = comboBoxIncomeAcc.Text;
                item.CostOfGoodsSoldAccount = comboBoxCostAcc.Text;
                item.Status = checkBoxStatus.Checked;
                item.Brand = Convert.ToInt32(comboBoxBrand.SelectedValue);

               
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            if (itemsBLL.Insert(item))
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("failed");
            }
        }
    }
}
