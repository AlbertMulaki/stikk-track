﻿namespace Wholesale_Management
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.itemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.itemCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brandsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newBrandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewBrandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.measurementUnitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.producersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taxRatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.typeOfGoodsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.locationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.banksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.citiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchasesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newPurchaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.financialReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incomeReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutTRACKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.stockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchasesInDetailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesInDetailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemsToolStripMenuItem,
            this.subjectsToolStripMenuItem,
            this.purchasesToolStripMenuItem,
            this.salesToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(841, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // itemsToolStripMenuItem
            // 
            this.itemsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemsToolStripMenuItem1,
            this.itemCategoryToolStripMenuItem,
            this.brandsToolStripMenuItem,
            this.measurementUnitsToolStripMenuItem,
            this.producersToolStripMenuItem,
            this.taxRatesToolStripMenuItem,
            this.typeOfGoodsToolStripMenuItem});
            this.itemsToolStripMenuItem.Name = "itemsToolStripMenuItem";
            this.itemsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.itemsToolStripMenuItem.Text = "Items";
            // 
            // itemsToolStripMenuItem1
            // 
            this.itemsToolStripMenuItem1.Name = "itemsToolStripMenuItem1";
            this.itemsToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.itemsToolStripMenuItem1.Text = "Items";
            this.itemsToolStripMenuItem1.Click += new System.EventHandler(this.itemsToolStripMenuItem1_Click);
            // 
            // itemCategoryToolStripMenuItem
            // 
            this.itemCategoryToolStripMenuItem.Name = "itemCategoryToolStripMenuItem";
            this.itemCategoryToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.itemCategoryToolStripMenuItem.Text = "Item Category";
            // 
            // brandsToolStripMenuItem
            // 
            this.brandsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newBrandToolStripMenuItem,
            this.viewBrandToolStripMenuItem});
            this.brandsToolStripMenuItem.Name = "brandsToolStripMenuItem";
            this.brandsToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.brandsToolStripMenuItem.Text = "Brands";
            // 
            // newBrandToolStripMenuItem
            // 
            this.newBrandToolStripMenuItem.Name = "newBrandToolStripMenuItem";
            this.newBrandToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.newBrandToolStripMenuItem.Text = "New Brand ...";
            this.newBrandToolStripMenuItem.Click += new System.EventHandler(this.newBrandToolStripMenuItem_Click);
            // 
            // viewBrandToolStripMenuItem
            // 
            this.viewBrandToolStripMenuItem.Name = "viewBrandToolStripMenuItem";
            this.viewBrandToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.viewBrandToolStripMenuItem.Text = "View Brand";
            this.viewBrandToolStripMenuItem.Click += new System.EventHandler(this.viewBrandToolStripMenuItem_Click);
            // 
            // measurementUnitsToolStripMenuItem
            // 
            this.measurementUnitsToolStripMenuItem.Name = "measurementUnitsToolStripMenuItem";
            this.measurementUnitsToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.measurementUnitsToolStripMenuItem.Text = "Measurement Units";
            // 
            // producersToolStripMenuItem
            // 
            this.producersToolStripMenuItem.Name = "producersToolStripMenuItem";
            this.producersToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.producersToolStripMenuItem.Text = "Producers";
            // 
            // taxRatesToolStripMenuItem
            // 
            this.taxRatesToolStripMenuItem.Name = "taxRatesToolStripMenuItem";
            this.taxRatesToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.taxRatesToolStripMenuItem.Text = "Tax rates";
            // 
            // typeOfGoodsToolStripMenuItem
            // 
            this.typeOfGoodsToolStripMenuItem.Name = "typeOfGoodsToolStripMenuItem";
            this.typeOfGoodsToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.typeOfGoodsToolStripMenuItem.Text = "Type of goods";
            // 
            // subjectsToolStripMenuItem
            // 
            this.subjectsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subjectToolStripMenuItem,
            this.locationsToolStripMenuItem,
            this.banksToolStripMenuItem,
            this.citiesToolStripMenuItem,
            this.statesToolStripMenuItem});
            this.subjectsToolStripMenuItem.Name = "subjectsToolStripMenuItem";
            this.subjectsToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.subjectsToolStripMenuItem.Text = "Subjects";
            // 
            // subjectToolStripMenuItem
            // 
            this.subjectToolStripMenuItem.Name = "subjectToolStripMenuItem";
            this.subjectToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.subjectToolStripMenuItem.Text = "Subject";
            // 
            // locationsToolStripMenuItem
            // 
            this.locationsToolStripMenuItem.Name = "locationsToolStripMenuItem";
            this.locationsToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.locationsToolStripMenuItem.Text = "Locations";
            // 
            // banksToolStripMenuItem
            // 
            this.banksToolStripMenuItem.Name = "banksToolStripMenuItem";
            this.banksToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.banksToolStripMenuItem.Text = "Banks";
            // 
            // citiesToolStripMenuItem
            // 
            this.citiesToolStripMenuItem.Name = "citiesToolStripMenuItem";
            this.citiesToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.citiesToolStripMenuItem.Text = "Cities";
            // 
            // statesToolStripMenuItem
            // 
            this.statesToolStripMenuItem.Name = "statesToolStripMenuItem";
            this.statesToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.statesToolStripMenuItem.Text = "States";
            // 
            // purchasesToolStripMenuItem
            // 
            this.purchasesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newPurchaseToolStripMenuItem});
            this.purchasesToolStripMenuItem.Name = "purchasesToolStripMenuItem";
            this.purchasesToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.purchasesToolStripMenuItem.Text = "Purchases";
            // 
            // newPurchaseToolStripMenuItem
            // 
            this.newPurchaseToolStripMenuItem.Name = "newPurchaseToolStripMenuItem";
            this.newPurchaseToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.newPurchaseToolStripMenuItem.Text = "New Purchase";
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSaleToolStripMenuItem});
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.salesToolStripMenuItem.Text = "Sales";
            // 
            // newSaleToolStripMenuItem
            // 
            this.newSaleToolStripMenuItem.Name = "newSaleToolStripMenuItem";
            this.newSaleToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.newSaleToolStripMenuItem.Text = "New Sale";
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.financialReportToolStripMenuItem,
            this.incomeReportToolStripMenuItem,
            this.stockToolStripMenuItem,
            this.purchasesInDetailToolStripMenuItem,
            this.salesInDetailToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // financialReportToolStripMenuItem
            // 
            this.financialReportToolStripMenuItem.Name = "financialReportToolStripMenuItem";
            this.financialReportToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.financialReportToolStripMenuItem.Text = "Financial Report";
            // 
            // incomeReportToolStripMenuItem
            // 
            this.incomeReportToolStripMenuItem.Name = "incomeReportToolStripMenuItem";
            this.incomeReportToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.incomeReportToolStripMenuItem.Text = "Income Report";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.infoToolStripMenuItem,
            this.aboutTRACKToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.infoToolStripMenuItem.Text = "Info";
            // 
            // aboutTRACKToolStripMenuItem
            // 
            this.aboutTRACKToolStripMenuItem.Name = "aboutTRACKToolStripMenuItem";
            this.aboutTRACKToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.aboutTRACKToolStripMenuItem.Text = "About TRACK";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 622);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(841, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(40, 17);
            this.toolStripStatusLabel1.Text = "Active";
            // 
            // stockToolStripMenuItem
            // 
            this.stockToolStripMenuItem.Name = "stockToolStripMenuItem";
            this.stockToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.stockToolStripMenuItem.Text = "Stock";
            // 
            // purchasesInDetailToolStripMenuItem
            // 
            this.purchasesInDetailToolStripMenuItem.Name = "purchasesInDetailToolStripMenuItem";
            this.purchasesInDetailToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.purchasesInDetailToolStripMenuItem.Text = "Purchases in detail";
            // 
            // salesInDetailToolStripMenuItem
            // 
            this.salesInDetailToolStripMenuItem.Name = "salesInDetailToolStripMenuItem";
            this.salesInDetailToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.salesInDetailToolStripMenuItem.Text = "Sales in detail";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Wholesale_Management.Properties.Resources.Track_Logo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(841, 644);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "TRACK: Wholesale Management";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem itemsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem itemCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brandsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem measurementUnitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem producersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taxRatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem typeOfGoodsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem locationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem banksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem citiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newBrandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewBrandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchasesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newPurchaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newSaleToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem financialReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incomeReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutTRACKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchasesInDetailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesInDetailToolStripMenuItem;
    }
}