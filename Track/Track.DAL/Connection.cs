﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Configuration;



namespace Track.DAL

{
    public class Connection
    {
       
        //"Data Source=LAPTOP\\SQLEXPRESS; Initial Catalog=Projekti; User ID=sa; Password=komando1234"
        private SqlConnection conn;
        private SqlDataAdapter adapter;
       
        public Connection()
        {
            adapter = new SqlDataAdapter();
            string connectionString = ConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString;
            conn = new SqlConnection( connectionString);
        }

        private SqlConnection openConnection()
        {
            if(conn.State == System.Data.ConnectionState.Broken || conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            return conn;
        }

        public DataTable Select(string _query,CommandType commandType,SqlParameter[] sqlParameters)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable dataTable = new DataTable();

            DataSet dataSet = new DataSet();
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = _query;
                cmd.CommandType = commandType;
                cmd.Parameters.AddRange(sqlParameters);
                cmd.ExecuteNonQuery();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);
                dataTable = dataSet.Tables[0];
            }
            catch (SqlException e)
            {
                
                throw new Exception("Connection.Select:" + e.StackTrace.ToString());
            } 
            return dataTable;
        }
        public int SelectScalarFunction(string _query, CommandType commandType)
        {
            SqlCommand cmd = new SqlCommand();
            int a = 0;

            
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = _query;
                cmd.CommandType = commandType;
                
                adapter.SelectCommand = cmd;
                a = Convert.ToInt32(cmd.ExecuteScalar());
         
            }
            catch (SqlException e)
            {
                
                throw new Exception("Connection.Select:" + e.StackTrace.ToString());
                
            }
           
            return a;
        }
        public DataTable Select(string _query, CommandType commandType )
        {
            SqlCommand cmd = new SqlCommand();
            DataTable dataTable = new DataTable();

            DataSet dataSet = new DataSet();
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = _query;
                cmd.CommandType = commandType;
                cmd.ExecuteNonQuery();
                adapter.SelectCommand = cmd;
                adapter.Fill(dataSet);
                dataTable = dataSet.Tables[0];
            }
            catch (SqlException e)
            {
                Console.Write("Connection.Select:" + e.StackTrace.ToString());
                throw new Exception("Connection.Select:" + e.StackTrace.ToString());
            }
            return dataTable;
        }
        public bool Insert(string _query,CommandType commandType, SqlParameter[] sqlParameters)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = _query;
                cmd.Parameters.AddRange(sqlParameters);
                cmd.CommandType = commandType;
                cmd.ExecuteNonQuery();
                adapter.InsertCommand = cmd;
               
            }
            catch(Exception e)
            {
                Console.Write("Connection.Insert:" + e.StackTrace.ToString());
                throw new Exception("Connection.Insert:" + e.StackTrace.ToString());
                
            }
       
            return true;

        }

        public bool Update(string _query,CommandType commandType, SqlParameter[] sqlParameters)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = _query;
                cmd.CommandType = commandType;
                cmd.Parameters.AddRange(sqlParameters);
                adapter.UpdateCommand = cmd;
                cmd.ExecuteNonQuery();
            }
            catch(Exception e)
            {
                Console.Write("Connection.Update:" + e.StackTrace.ToString());
                throw new Exception("Connection.Update:" + e.StackTrace.ToString());
         
            }
            return true;
        }
        public bool Delete(string _query, CommandType commandType, SqlParameter[] sqlParameters)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = _query;
                cmd.CommandType = commandType;
                cmd.Parameters.AddRange(sqlParameters);
                adapter.DeleteCommand = cmd;
                cmd.ExecuteNonQuery();
            }catch(Exception e)
            {
                Console.Write("Connection.Delete:" + e.StackTrace.ToString());
                throw new Exception("Connection.Delete:" + e.StackTrace.ToString());
               
            }
            return true;
        }

    }
}
