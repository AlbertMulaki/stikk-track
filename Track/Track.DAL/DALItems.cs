﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Track.BOL;
using System.Data.SqlClient;
using System.Data;

namespace Track.DAL
{
    public class DALItems
    {
        Connection conn = new Connection();
        private Items _item;
        public Items Item
        {
            get
            {
                return _item;
            }
            set
            {
                _item = value;
            }
        }

        public DALItems():base()
        {
            
        }
       /* public bool Insert(Items item)
        {
            
           
            string query = "usp_Items_Insert";
            SqlParameter[] sqlParameters = new SqlParameter[17];
            sqlParameters[0] = new SqlParameter("@prmBarcode", SqlDbType.NVarChar);
            sqlParameters[0].Value = Convert.ToString(item.Barcode);

            sqlParameters[1] = new SqlParameter("@prmName", SqlDbType.NVarChar);
            sqlParameters[1].Value = Convert.ToString(item.Name);

            sqlParameters[2] = new SqlParameter("@prmBasicMeasuringUnitID", SqlDbType.Int);
            sqlParameters[2].Value = Convert.ToString(item.BasicMeasuringUnitID);

            sqlParameters[3] = new SqlParameter("@prmSubCategory", SqlDbType.Int);
            sqlParameters[3].Value = Convert.ToString(item.SubCategoryID);

            sqlParameters[4] = new SqlParameter("@prmPackage", SqlDbType.Decimal);
            sqlParameters[4].Value = Convert.ToString(item.Package);

            sqlParameters[5] = new SqlParameter("@prmNetWeight", SqlDbType.Decimal);
            sqlParameters[5].Value = Convert.ToString(item.NetWeight);

            sqlParameters[6] = new SqlParameter("@prmGrossWeight", SqlDbType.Decimal);
            sqlParameters[6].Value = Convert.ToString(item.GrossWeight);

            sqlParameters[7] = new SqlParameter("@prmLength", SqlDbType.Decimal);
            sqlParameters[7].Value = Convert.ToString(item.Length);

            sqlParameters[8] = new SqlParameter("@prmWidth", SqlDbType.Decimal);
            sqlParameters[8].Value = Convert.ToString(item.Width);

            sqlParameters[9] = new SqlParameter("@prmHeight", SqlDbType.Decimal);
            sqlParameters[9].Value = Convert.ToString(item.Height);

            sqlParameters[10] = new SqlParameter("@prmTypeOfGoods", SqlDbType.Int);
            sqlParameters[10].Value = Convert.ToString(item.TypeOfGoods);

            sqlParameters[11] = new SqlParameter("@prmTaxRates", SqlDbType.NVarChar);
            sqlParameters[11].Value = Convert.ToString(item.TaxRates);

            sqlParameters[12] = new SqlParameter("@prmExpenseAccount", SqlDbType.NVarChar);
            sqlParameters[12].Value = Convert.ToString(item.ExpenseAccount);

            sqlParameters[13] = new SqlParameter("@prmSalesAccount", SqlDbType.NVarChar);
            sqlParameters[13].Value = Convert.ToString(item.SalesAccount);

            sqlParameters[14] = new SqlParameter("@prmCostOfGoodsSoldAccount", SqlDbType.NVarChar);
            sqlParameters[14].Value = Convert.ToString(item.CostOfGoodsSoldAccount);

            sqlParameters[15] = new SqlParameter("@prmStatus", SqlDbType.Bit);
            sqlParameters[15].Value = Convert.ToString(item.Status);

            sqlParameters[16] = new SqlParameter("@prmBrand", SqlDbType.Int);
            sqlParameters[16].Value = Convert.ToString(item.Brand);





           /* List<SqlParameter> sqlParam = new List<SqlParameter>();
            sqlParam.Add(new SqlParameter("@prmBarcode", item.Barcode));
            sqlParam.Add(new SqlParameter("@prmName", item.Name));
            sqlParam.Add(new SqlParameter("@prmBasicMeasuringUnitID", item.BasicMeasuringUnitID));
            sqlParam.Add(new SqlParameter("@prmSubCategory", item.SubCategoryID));
            sqlParam.Add(new SqlParameter("@prmPackage", item.Package));
            sqlParam.Add(new SqlParameter("@prmNetWeight",item.NetWeight));
            sqlParam.Add(new SqlParameter("@prmGrossWeight", item.GrossWeight));
            sqlParam.Add(new SqlParameter("@prmLength", item.Length));
            sqlParam.Add(new SqlParameter("@prmWidth", item.Width));
            sqlParam.Add(new SqlParameter("@prmHeight", item.Height));
            sqlParam.Add(new SqlParameter("@prmTypeOfGoods", item.TypeOfGoods));
            sqlParam.Add(new SqlParameter("@prmTaxRates", item.TaxRates));
            sqlParam.Add(new SqlParameter("@prmExpenseAccount", item.ExpenseAccount));
            sqlParam.Add(new SqlParameter("@prmSalesAccount", item.SalesAccount));
            sqlParam.Add(new SqlParameter("@prmCostOfGoodsSoldAccount", item.CostOfGoodsSoldAccount));
            sqlParam.Add(new SqlParameter("@prmStatus", item.Status));
            sqlParam.Add(new SqlParameter("@prmBrand", item.Brand));
            

            return conn.Insert(query,System.Data.CommandType.StoredProcedure, sqlParameters);
        }*/
        

        public DataTable GetBrands()
        {
            string query = "usp_Brand_Select_All";
            return conn.Select(query, CommandType.StoredProcedure);
        }
        public DataTable GetUnits()
        {
            string query ="usp_Units_Select_All";
            return conn.Select(query, CommandType.StoredProcedure);
        }

        public DataTable GetTypeOfGoofs()
        {
            string query = "usp_TypeOfGoods_Select_All";
            return conn.Select(query, CommandType.StoredProcedure);
        }
        public DataTable GetTaxRates()
        {
            string query = "usp_TaxRates_Select_All";
            return conn.Select(query, CommandType.StoredProcedure);
        }

        public DataTable GetItemCategory()
        {
            string query = "usp_ItemCategory_Select_All";
            return conn.Select(query, CommandType.StoredProcedure);
        }
        public DataTable GetSubCategory()
        {
            string query = "usp_SubCategory_Select_All";
            return conn.Select(query, CommandType.StoredProcedure);
        }
       
        public DataTable GetAccount()
        {
            string query = "usp_AccountingPlan_Select_All";
            return conn.Select(query,CommandType.StoredProcedure);
        }

        public int GetMaxID()
        {

            return conn.SelectScalarFunction("Select fn_GetItemsMaximumID()", System.Data.CommandType.Text);
        }
    }
}
