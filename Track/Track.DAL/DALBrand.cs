﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Track.BOL;
using System.Data.SqlClient;
using System.Data;


namespace Track.DAL
{
    public class DALBrand
    {
        //Connection connString;
        Connection conn;

       /* public void Insert(Brand brand)
        {
            connString = new Connection();
            SqlConnection conn = new SqlConnection(connString.ConnectionString);
            try
            {
                SqlCommand cmd = new SqlCommand("usp_Brand_Insert", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@prmCode", brand.BrandID);
                cmd.Parameters.AddWithValue("@prmName", brand.Name);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("DALBRands.Insert: " + ex.Message);
            }
            finally
            {
                if(conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            


        }*/

       /* public List<Brand> SelectAll()
        {
            List<Brand> listBrands = new List<Brand>();
            connString = new Connection();
            SqlConnection conn = new SqlConnection(connString.ConnectionString);

            try
            {
                SqlCommand cmd = new SqlCommand("usp_Brand_Select_All",conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                conn.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())//var foo = new Bar();try{Baz();} finally { foo.Dispose(); }
                {
                    while (reader.Read())
                    {
                        Brand brand = new Brand();
                        brand.BrandID = reader["BrandID"].ToString();
                        brand.Name = reader["Name"].ToString();
                        listBrands.Add(brand);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("DALBrand.SelectAll: " + ex.Message);
            }
            finally
            {
                if(conn.State != System.Data.ConnectionState.Broken)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return listBrands;
        }*/
        
        /*public  List<Brand> Select_All_Filtered(string search)
        {
            List<Brand> listBrand = new List<Brand>();
            Connection connString = new Connection();
            SqlConnection conn = new SqlConnection(connString.ConnectionString);
            try
            {
                SqlCommand cmd = new SqlCommand("usp_Brand_Select_All_Filtered", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                
                cmd.Parameters.AddWithValue("@prmCode", search);
                cmd.Parameters.AddWithValue("@prmName", search);
                conn.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Brand brand = new Brand();
                        brand.BrandID = reader["BrandID"].ToString();
                        brand.Name = reader["Name"].ToString();
                        listBrand.Add(brand);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("DALBrand.SelectAllFiltered: " + ex.Message);
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Broken)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return listBrand;
        }*/


        public DALBrand() 
        {
            conn = new Connection();
        }

        public DataTable Select()
        {
            string query = string.Format("usp_Brand_Select_All");

            return conn.Select(query, CommandType.StoredProcedure);

        }

    }
}
