﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Track.BOL
{
    public class Items
    {
        private int itemID;
        private string barcode;
        private string name;
        private int basicMeasuringUnitID;
        private int subCategoryID;
        private double package;
        private double netWeight, grossWeight, length, width, height;
        private int typeOfGoods;
        private string taxRates, expenseAccount, salesAccount, costOfGoodsSoldAccount;
        private bool status;
        private int brand;

        public Items(string name, string barcode,  int basicMeasuringUnitID, int subCategoryID, double package, double netWeight, double grossWeight, double length, double width, double height, int typeOfGoods, string taxRates, string expenseAccount, string salesAccount, string costOfGoodsSoldAccount, bool status, int brand)
        {
            
            Barcode = barcode;
            Name = name;
            BasicMeasuringUnitID = basicMeasuringUnitID;
            SubCategoryID = subCategoryID;
            Package = package;
            NetWeight = netWeight;
            GrossWeight = grossWeight;
            Length = length;
            Width = width;
            Height = height;
            TypeOfGoods = typeOfGoods;
            TaxRates = taxRates;
            ExpenseAccount = expenseAccount;
            SalesAccount = salesAccount;
            CostOfGoodsSoldAccount = costOfGoodsSoldAccount;
            Status = status;
            Brand = brand;
        }
        public Items()
        {

        }

        public int ItemID
        {
            get
            {
                return itemID;
            }

            set
            {
                itemID = value;
            }
        }

        public string Barcode
        {
            get
            {
                return barcode;
            }

            set
            {
                barcode = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public int BasicMeasuringUnitID
        {
            get
            {
                return basicMeasuringUnitID;
            }

            set
            {
                basicMeasuringUnitID = value;
            }
        }

        public int SubCategoryID
        {
            get
            {
                return subCategoryID;
            }

            set
            {
                subCategoryID = value;
            }
        }

        public double Package
        {
            get
            {
                return package;
            }

            set
            {
                package = value;
            }
        }

        public double NetWeight
        {
            get
            {
                return netWeight;
            }

            set
            {
                netWeight = value;
            }
        }

        public double GrossWeight
        {
            get
            {
                return grossWeight;
            }

            set
            {
                grossWeight = value;
            }
        }

        public double Length
        {
            get
            {
                return length;
            }

            set
            {
                length = value;
            }
        }

        public double Width
        {
            get
            {
                return width;
            }

            set
            {
                width = value;
            }
        }

        public double Height
        {
            get
            {
                return height;
            }

            set
            {
                height = value;
            }
        }

        public int TypeOfGoods
        {
            get
            {
                return typeOfGoods;
            }

            set
            {
                typeOfGoods = value;
            }
        }

        public string TaxRates
        {
            get
            {
                return taxRates;
            }

            set
            {
                taxRates = value;
            }
        }

        public string ExpenseAccount
        {
            get
            {
                return expenseAccount;
            }

            set
            {
                expenseAccount = value;
            }
        }

        public string SalesAccount
        {
            get
            {
                return salesAccount;
            }

            set
            {
                salesAccount = value;
            }
        }

        public string CostOfGoodsSoldAccount
        {
            get
            {
                return costOfGoodsSoldAccount;
            }

            set
            {
                costOfGoodsSoldAccount = value;
            }
        }

        public bool Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public int Brand
        {
            get
            {
                return brand;
            }

            set
            {
                brand = value;
            }
        }
    }
}
