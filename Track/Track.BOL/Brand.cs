﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Track.BOL
{
    public class Brand
    {
        private string brandID;
        private string name;

        public string BrandID
        {
            get
            {
                return brandID;
            }
            set
            {
                brandID = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }



    }
}
