﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Track.BOL
{
     public class User
    {
        private string username;
        private string name;
        private string surname;
        private string userPassword;
        private short group;
        private bool status;

        
        public string Username { get { return username; } set { username = value;} }
        public string Name { get { return name; } set { name = value; } }
        public string Surname { get { return surname; } set { surname = value; } }
        public string UserPassword { get { return userPassword; } set { userPassword = value; } }
        public short Group { get { return group; } set { group = value; } }
        public bool Status { get { return status; } set { status = value; } }

        public User(string username,string name,string surname, string userPassword, short group,bool status)
        {
            Username = username;
            Name = name;
            Surname = surname;
            UserPassword = userPassword;
            Group = Group;
            Status = status;
        }

        public User() { }

    }
}
